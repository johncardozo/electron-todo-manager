const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

function crearVentanaNuevaTarea() {
  // Crea la ventana de una nueva tarea
  let ventanaNuevaTarea = new BrowserWindow({
    width: 400,
    height: 300,
    title: "Nueva tarea",
    webPreferences: {
      // Permite utilizar require en los scripts de las páginas
      nodeIntegration: true,
    },
  });

  // Elimina el menu por defecto. SOLO WINDOWS Y LINUX
  ventanaNuevaTarea.setMenu(null);

  // Carga del archivo "nuevaTarea.html"
  ventanaNuevaTarea.loadURL(
    url.format({
      pathname: path.join(__dirname, "../templates/nuevaTarea.html"),
      protocol: "file",
      slashes: true,
    })
  );

  return ventanaNuevaTarea;
}

module.exports = { crearVentanaNuevaTarea };
