const { app, BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

let ventanaPrincipal;

function crearVentanaPrincipal() {
  // Creación de la ventana principal
  ventanaPrincipal = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      // Permite utilizar require en los scripts de las páginas
      nodeIntegration: true,
    },
  });

  // Carga del archivo "index.html"
  ventanaPrincipal.loadURL(
    url.format({
      pathname: path.join(__dirname, "../templates/index.html"),
      protocol: "file",
      slashes: true,
    })
  );

  // Escucha el evento de cierre de la ventana principal
  ventanaPrincipal.on("closed", () => {
    // Cierra la aplicación
    app.quit();
  });

  return ventanaPrincipal;
}

module.exports = { crearVentanaPrincipal };
