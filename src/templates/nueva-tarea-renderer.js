const { ipcRenderer } = require("electron");
const { v4: uuidv4 } = require('uuid');

// Obtiene el boton
let boton = document.getElementById("boton");
boton.addEventListener("click", (evento) => {
  // Previene el envio del formulario
  evento.preventDefault();
  // Obtiene el nombre de la tarea
  let tarea = document.getElementById("tarea").value;
  // Crea el objeto que será enviado
  const datos = {
    id: uuidv4(),
    tarea: tarea,
    terminada: false
  };
  // Envio de los datos al proceso principal
  ipcRenderer.send("nueva-tarea", datos);
});
