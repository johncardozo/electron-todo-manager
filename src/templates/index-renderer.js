const { ipcRenderer } = require("electron");

// Escuchando el evento "nueva-tarea"
ipcRenderer.on("nueva-tarea", (evento, datos) => {
  // Muestra la tarea en la página
  mostrarTarea(datos.tarea, false);
});

ipcRenderer.on("mostrar-tareas", (evento, datos) => {
  // Recorre las tareas
  Array.from(datos).forEach((tarea) => {
    // Muestra la tarea en la página
    mostrarTarea(tarea.tarea, tarea.terminada);
  });
});

let botonNuevaTarea = document.getElementById("botonNuevaTarea");

botonNuevaTarea.addEventListener("click", () => {
  // Envio de los datos al proceso principal
  ipcRenderer.send("crear-ventana-nueva-tarea");
});

function checkTarea(evento) {
  // Obtiene el checkbox al cual se le hizo click
  let checkbox = evento.target;
  // Obtiene el padre <li>
  let liPadre = checkbox.parentElement;
  // Cambia la clase del <li>
  liPadre.classList.toggle("terminada");
}

function clickBotonEliminar(evento) {
  // Obtiene el button al cual se le hizo click
  let button = evento.target;
  // Obtiene el padre <li>
  let liPadre = button.parentElement;
  // Eliminar el li padre
  liPadre.remove();
}

function mostrarTarea(titulo, terminada) {
  // Obtiene la lista de tareas
  let tareas = document.getElementById("tareas");
  // Crea un elemento <li>
  let liElemento = document.createElement("li");
  // Crea un element input[type=checkbox]
  let checkbox = document.createElement("input");
  checkbox.setAttribute("type", "checkbox");
  // Verifica si la tarea está terminada
  if (terminada) {
    checkbox.setAttribute("checked", "checked");
  }
  // Agrega el event listener al checkbox
  checkbox.addEventListener("click", checkTarea);
  // Crea el element de texto <span>
  let spanElemento = document.createElement("span");
  spanElemento.textContent = titulo;
  // Crea el boton para eliminar la tarea
  let botonEliminar = document.createElement("button");
  botonEliminar.textContent = "x";
  botonEliminar.addEventListener("click", clickBotonEliminar);
  // Agrega los elementos al <li>
  liElemento.append(checkbox, spanElemento, botonEliminar);
  // Agrega el elemento a la lista
  tareas.appendChild(liElemento);
}
