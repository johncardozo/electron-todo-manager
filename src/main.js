// Librerías
const { app, Menu, ipcMain } = require("electron");
const path = require("path");
const fs = require("fs");
require("dotenv").config();

const { crearVentanaNuevaTarea } = require("./views/nuevaTarea");
const { crearVentanaPrincipal } = require("./views/index");

// Verifica que la aplicación no esté empaquetada o que esté en ambiente de desarrollo
if (!app.isPackaged || process.env.NODE_ENV === "development") {
  require("electron-reload")(__dirname, {
    // Revisa el proceso Electron
    electron: path.join(__dirname, "node_modules", ".bin", "electron"),
  });
}

let ventanaPrincipal;
let ventanaNuevaTarea;
let tareas;
let rutaArchivo = app.getAppPath() + "/data/tareas.json";

// Evento "ready" de la aplicación
app.on("ready", () => {
  // Carga las tareas del archivo JSON
  cargarTareas();

  // Crea la ventana principal
  ventanaPrincipal = crearVentanaPrincipal();

  // Carga el menu de un template
  const menuPrincipal = Menu.buildFromTemplate(templateMenu);
  Menu.setApplicationMenu(menuPrincipal);

  // Verifica que la ventana principal haya cargado el DOM
  ventanaPrincipal.webContents.once("dom-ready", () => {
    // Envía el mensaje para que se muestren todas las tareas
    ventanaPrincipal.webContents.send("mostrar-tareas", tareas);
  });
});

// Escucha el evento "nueva-tarea"
ipcMain.on("nueva-tarea", (evento, tarea) => {
  // Agrega la tarea
  agregarTarea(tarea);

  // Envía los tarea a index.html
  ventanaPrincipal.webContents.send("nueva-tarea", tarea);

  // Cierra la ventana
  ventanaNuevaTarea.close();
});

// Escucha el evento "crear-ventana-nueva-tarea"
ipcMain.on("crear-ventana-nueva-tarea", () => {
  // Crea la ventana para una nueva tarea
  ventanaNuevaTarea = crearVentanaNuevaTarea();
});

// Menu principal
const templateMenu = [
  {
    label: "Tareas",
    submenu: [
      {
        label: "Nueva tarea",
        accelerator: "Ctrl+N",
        click() {
          // Crea la ventana para una nueva tarea
          ventanaNuevaTarea = crearVentanaNuevaTarea();
        },
      },
      {
        label: "Eliminar todas las tareas",
        click() {
          console.log("Eliminar todas las tareas...");
        },
      },
      {
        label: "Salir",
        accelerator: process.platform === "darwin" ? "command+Q" : "Ctrl+Q",
        click() {
          app.quit();
        },
      },
    ],
  },
];

// Verifica si el SO es Mac para quitar
// el nombre de la aplicación del menú
if (process.platform === "darwin") {
  templateMenu.unshift({
    label: app.getName(),
  });
}

// Verifica que la aplicación no esté empaquetada o que esté en ambiente de desarrollo
if (!app.isPackaged && process.env.NODE_ENV === "development") {
  // Agrega una nueva opción al menú
  templateMenu.push({
    label: "DevTools",
    submenu: [
      {
        label: "Mostrar/Ocultar Dev Tools",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        },
      },
      {
        role: "reload",
      },
    ],
  });
}

function cargarTareas() {
  // Verifica si el archivo de datos existe
  if (fs.existsSync(rutaArchivo)) {
    // Abre el archivo
    let archivo = fs.readFileSync(rutaArchivo);
    // Carga la información del archivo en memoria
    tareas = JSON.parse(archivo);
  } else {
    // TODO: Verificar en qué lugar del disco se guarda el archivo cuando se ejecuta la app en producción.
    // Crea el archivo
    fs.createWriteStream(rutaArchivo);
    // Inicializa la lisat de tareas
    tareas = [];
  }
}

function agregarTarea(tarea) {
  // Agrega la tarea a los datos locales
  tareas.push(tarea);

  // Convierte los datos en string
  let datos = JSON.stringify(tareas);

  // Escribe las tareas al archivo
  fs.writeFileSync(rutaArchivo, datos);
}
